app.controller('LoginController', function($location, localStorageService) {
	var self = this;
	this.storage = localStorageService;

	this.loginUser = function() {
		var key = self.username;
		var pass = self.password;
		var data = angular.fromJson(self.storage.get(key));
		
		// Check if user exist in storage
		if (data !== null) {
			self.loginError = null;
			var realPass = data.password;

			// Check if pass match
			if(pass == realPass) {
				self.storage.set('username', key);
				$location.path('/profile');
			} else {
				self.passwordError = "Wrong password";
				self.loginForm.$setPristine();
			}
		} else {
			self.loginError = "Oops, we can't find this username";
			self.passwordError = null;
			self.loginForm.$setPristine();
		}
	};
});
