app.controller('EditProfileController', function($location, localStorageService) {
	var self = this;
	this.storage = localStorageService;
	
	// defining loged in username
	this.username = self.storage.get('username');
	
	// getting this username data
	this.data = angular.fromJson(self.storage.get(self.username));

	// Set data from storage
	this.username = self.data.username;
	this.email = self.data.email;
	this.country = self.data.country;
	this.city = self.data.city;
	this.postcode = self.data.postcode;
	this.address = self.data.address;

	// setting avatar func
	function setAvatar() {
		// Check if input file has new img
		var uploadImg = check();
		function check() {
			if (self.avatar !== undefined) {
				var newImg = 'data:' + self.avatar.filetype + ';base64,' + self.avatar.base64;
				return newImg;
			}
		}
		if (uploadImg) return uploadImg;

		// Check if img exist in storage
		var dataImg = self.data.avatar;
		if (dataImg) return dataImg;

		// set no avatar img
		var noImg = '../img/no-img.png';
		return noImg;
	}

	this.showAvatar = setAvatar();

	this.updateAvatar = function() {
		self.showAvatar = setAvatar();
	};

	this.changeData = function() {
		var data = {};
		if (self.username != self.data.username){
			var checkNewUser = self.storage.get(self.username);
			if(checkNewUser !== null) {
				self.usernameError = true;
				return;
			}
			self.storage.set('username', self.username);
			self.storage.remove(self.data.username);
			data.username = self.username;
		} else {
			data.username = self.username;
		}
		if(self.email) data.email = self.email;
		if(self.country) data.country = self.country;
		if(self.city) data.city = self.city;
		if(self.postcode) data.postcode = self.postcode;
		if(self.address) data.address = self.address;
		if (self.avatar) {
			data.avatar = 'data:' + self.avatar.filetype + ';base64,' + self.avatar.base64;
		} else if (self.data.avatar) {
			data.avatar = self.data.avatar;
		}

		// If password was changed setting up new password
		if (self.editForm.password.$dirty || self.editForm.oldPassword.$dirty || self.editForm.password2.$dirty) {
			if(self.password.length || self.oldPassword.length || self.password2.length){
				//	Check rightness of old password 
				if (self.oldPassword != self.data.password) {
					self.oldPasswordError = true;
					self.editForm.oldPassword.$setPristine();
					return;
				}

				data.password = self.password;
			}
		} else {
			data.password = self.data.password;
		}

		self.storage.set(self.username, angular.toJson(data));
		console.log('changed: ' + self.username);
		console.log(data);
		$location.path('/profile');
	};
});
