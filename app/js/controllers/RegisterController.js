app.controller('RegisterController', function($location, localStorageService) {
	var self = this;
	this.storage = localStorageService;

	this.registerUser = function() {
		// Check if user already exist and throw error
		var oldData = self.storage.get(self.username);
		if (oldData !== null) {
			self.loginError = "User already exist pick another one or log in";
			self.registerForm.username.$setPristine();
			return;
		}

		// Pack data
		var data = {
			username: self.username,
			email: self.email,
			password: self.password
		};

		// Set data to localstorage and redirect
		self.storage.set(self.username, angular.toJson(data));
		console.log('saved: ' + self.username);
		$location.path('/profile');
	};
});
