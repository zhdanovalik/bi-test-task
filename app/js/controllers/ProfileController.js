app.controller('ProfileController', function($location, localStorageService) {
	var self = this;
	this.storage = localStorageService;
	
	// defining loged in username
	this.username = self.storage.get('username');
	
	// getting this username data
	this.data = angular.fromJson(self.storage.get(self.username));

	// func to set progress bar
	this.setProgress = function () {
		var data = ((Object.keys(self.data).length - 1) / 7)*100;
		return Math.round(data);
	};
	this.progress = self.setProgress();

	this.logOut = function() {
		self.storage.remove('username');
		$location.path('/login');

	};

	this.deleteProfile = function() {
		var areYouSure = confirm('Are you want to delete your profile ?')
		if (areYouSure) {
			self.storage.remove('username', self.username);
			$location.path('/login');
		}
	};
});
