var app = angular.module("myApp", ["ngRoute", "LocalStorageModule", "naif.base64"]);

app.config(config);
app.run(run);

function config ($routeProvider) {
    $routeProvider

    .when('/login', {
        templateUrl: 'views/login.html'
    })

    .when('/register', {
        templateUrl: 'views/register.html'
    })

    // .when('/', {
    //     templateUrl: 'views/profile.html'
    // })

    .when('/profile', {
        templateUrl: 'views/profile.html'
    })

    .when('/edit-profile', {
        templateUrl: 'views/edit-profile.html'
    })

    .otherwise({ redirectTo: '/login' });
}


function run ($rootScope, $location, localStorageService) {

    this.storage = localStorageService;

    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        
        var username = self.storage.get('username');
        var restrictedPage = $location.path();

        if (!(restrictedPage == '/login' || restrictedPage == '/register') && !username) {
            $location.path('/login');
        } else {
            if ((restrictedPage == '/login' || restrictedPage == '/register') && username) {
                $location.path('/profile');
            }
        }
    });
}