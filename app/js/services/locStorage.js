app.factory('locStorage', function(localStorageService){
	var storage = localStorageService;
	return {
		getStorage: function(key) {
			return storage.get(key);
		},
		setStorage: function(key, val) {
			return storage.set(key, val);
		},
		removeStorage: function(key) {
			return storage.remove(key);
		},
		resetStorage: function() {
			return storage.clearAll();
		}
	};
});
