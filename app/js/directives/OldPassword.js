app.directive("oldPassword", function () {
  return {
    require: "ngModel",
    scope: false,
    link: function(scope, element, attributes, ngModel) {

      ngModel.$validators.oldPassword = function(modelValue) {
        return modelValue == scope.data.password;
      };

      scope.$watch("otherModelValue", function() {
        ngModel.$validate();
      });
    }
  };
});