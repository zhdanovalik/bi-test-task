# README
[Ali Zhdanov](https://cz.linkedin.com/in/ali-zhdanov-27178065) test, made for Bohemian Interactive.
Live demo: [http://alikzhdanov.esy.es/bi/](http://alikzhdanov.esy.es/bi/)

## Installation

* Run `npm install`

## Running

* `gulp serve` - run local server
* `gulp watch` - run local server and live compile
* `gulp` - simple build
