'use strict';

var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var reload =  browserSync.reload;
var plumber = require('gulp-plumber');
var notify = require("gulp-notify");
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var cssnano = require('gulp-cssnano');



var paths = {
    scripts: {
        src: 'app/js/',
        dest: 'app/js/'
    },
    styles: {
        src: 'app/sass/',
        dest: 'app/css/'
    },
};

var appFiles = {
    styles: paths.styles.src + 'style.scss',
    stylesWatch: paths.styles.src + '**/*.scss',
};


gulp.task('styles', function() {
	return gulp.src(appFiles.styles)
		.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
		// .pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(autoprefixer({browsers: ['> 1%', 'IE 9']}))
		.pipe(cssnano())
		// .pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(paths.styles.dest))
		.pipe(browserSync.stream())
		.pipe(notify("CSS Compiled"));
});

gulp.task('scripts', function() {
	var files = [
		paths.scripts.src + 'vendor/angular.min.js',
		paths.scripts.src + 'vendor/*.js',
		paths.scripts.src + 'main/*.js',
		paths.scripts.src + 'services/*.js',
		paths.scripts.src + 'controllers/*.js',
		paths.scripts.src + 'directives/*.js'
	];
	return gulp.src(files)
		// .pipe(sourcemaps.init())
		.pipe(concat('index.js'))
		.pipe(uglify({
			mangle: false
		}))
		// .pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(paths.scripts.dest));
});


gulp.task('watch',['styles', 'scripts'], function(){

    browserSync.init({
        server: "./app"
    });

    gulp.watch('app/sass/**/*.scss', ['styles']);
    gulp.watch(['app/js/**/*.js', '!app/js/index.js'], ['scripts']);

	gulp.watch(["app/index.html", "app/views/*.html"]).on('change', browserSync.reload);
	gulp.watch("app/js/index.js").on('change', browserSync.reload);
});

gulp.task('default',['scripts', 'styles'], function() {
    console.log('***build finished correct***');
});

gulp.task('serve', ['scripts', 'styles'], function() {
	browserSync.init({
        server: "./app"
    });
})
